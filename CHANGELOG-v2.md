# Changlog for Escrow-RDE-Client v2
All notable changes of the Escrow-RDE-Client v2 release series are documented in this file using the [Keep a CHANGELOG](http://keepachangelog.com/) principles.
### Untracked

## [2.0.1]
- Use real RFC3339 constant to accept date like "2021-08-11T18:12:15+02:00"
- Debug:Invalid hash file name for previous days
## [2.0.0] - 2020-09-21
- use UTC as default rather than the current local time to avoid validator issues