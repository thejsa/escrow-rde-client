# escrow-rde-client
The escrow-rde-client tool helps customers of Data Escrow Agents to validate, prepare and upload their deposits.

This tool is to be employed within [ICANN's Registrar Data Escrow Program](https://www.icann.org/resources/pages/registrar-data-escrow-2015-12-01-en), has been developed by [DENIC eG](https://www.denic.de/escrow) for its own use but can be used with any other Escrow Agent.

It will take care of the following tasks:

* validate the RDE deposit files according to ICANN validation rules,
* create hashfile over the content of the delivery,
* compress, encrypt and sign the deposit files, and
* upload the files to the Data Escrow Agent.

## Latest releases
[\_\_release\_linux\_\_](releases/\_\_release\_linux\_\_)

[\_\_release\_linux\_\_.sha256sum](releases/\_\_release\_linux\_\_.sha256sum)

[\_\_release\_windows\_\_](releases/\_\_release\_windows\_\_)

[\_\_release\_windows\_\_.sha256sum](releases/\_\_release\_windows\_\_.sha256sum)

## Source code
[\_\_release\_source\_\_](releases/\_\_release\_source\_\_)

[\_\_release\_source\_\_.sha256sum](releases/\_\_release\_source\_\_.sha256sum)

Contributions are welcome at <https://gitlab.com/team-escrow/escrow-rde-client>

### License
escrow-rde-client is licensed under the terms of the GNU Lesser General Public License v3.0.
See the repositories' [LICENSE](https://gitlab.com/team-escrow/escrow-rde-client/raw/master/LICENSE) file for further information.

## Prerequisites
* Your Escrow Agent must have set up your public GPG- and your public SSH-key parts along with your account

The escrow-rde-client will need access to the following files:

* Your private (encrypted) GPG key to sign the deposit files
* Your private SSH key for authentication at the sFTP service
* The public GPG key of the Escrow Agent to encrypt the deposit files

## Download the client

Download the client package for your platform (linux/windows) using the links above and extract it to a desired folder.

## Prepare the configuration file

* Generate a configuration file with ```./escrow-rde-client -i```
* rename the generated configuration file to ```config.yaml``` (optional)
* edit the parameters in the configuration file

## Prepare your deposit files
* to upload a deposit for the **current day (UTC time)** the source filenames (.csv) can have any filename as long
as the name includes `full`, `inc` and `hdl` (denoting full-deposit, incremental deposit and handle file)
* to upload a deposit for any past day, the filenames must follow this convention:

```
<IANAID>_RDE_<YYYY>-<MM>-<DD>_full.csv (or) <IANAID>_RDE_<YYYY>-<MM>-<DD>_inc.csv
<IANAID>_RDE_<YYYY>-<MM>-<DD>_hdl.csv
```

Replace your IANA ID, and year/month/day.

### Splitted deposit files
Source files with more than 1 million rows or 1 gigabyte of size need to be splitted into a sequence of files. The consecutive sequence numbers start with 1 and are contained in the filename by the following convention:

```
<IANAID>_RDE_<YYYY>-<MM>-<DD>_full_#.csv (or) <IANAID>_RDE_<YYYY>-<MM>-<DD>_inc_#.csv
<IANAID>_RDE_<YYYY>-<MM>-<DD>_hdl_#.csv
```

The symbol # is replaced by the corresponding sequence numbers. Only the first file should contain row headers, all other files just start with the successive content lines. To enable splitted deposits you need to update your configuration file:

```
multi: true
```

## Run the client

```
./escrow-rde-client -c config.yaml
```

You can additionally use the `-s` flag to only print the last validation error message. To print validation output directly to a file, add the following configuration line to your yaml config file:

```
logFile: /path/to/your/log/file
```

## Troubleshooting

This section discusses some known problems and possible solutions.

### Crashes due to out-of-memory errors
Out-of-memory errors can cause the application to crash without further error messages. If you run low on memory during computation, consider activating the file system cache in the configuration file:

```
useFileSystemCache: true
```

This will drastically reduce the amount of memory required but also increases computation time.

You can report problems with this tool via e-mail to escrow@denic.de

## Example data
Single-file full deposit, with handle references, *full.csv*. These examples may be splitted according the conventions of multi-file deposits (see above).

```
"domain","ns1","ns2","ns3","ns4","expiration_date","rt-handle","tc-handle","ac-handle","bc-handle","prt-handle","ptc-handle","pac-handle","pbc-handle"
"foobar0.test","ns1.dns.test","ns2.dns.test","ns3.dns.test","ns4.dns.test","2018-05-30 12:30:45","foo-123","foo-123","foo-123","foo-123","foo-123","foo-123","foo-123","foo-123"
"foobar1.test","ns1.dns.test","ns2.dns.test","ns3.dns.test","ns4.dns.test","2018-08-30T12:30:45Z","foo-123","foo-123","foo-123","foo-123","foo-123","foo-1235","foo-123","foo-123"
"foobar2.test","ns1.dns.test","ns2.dns.test","ns3.dns.test","ns4.dns.test","2018-05-30T12:30:45Z","foo-123","foo-123","foo-123","foo-123 foo-1234","foo-123","foo-1235","foo-123","foo-123"
"foobar3.test","ns1.dns.test","ns2.dns.test","ns3.dns.test","ns4.dns.test","2018-05-30 12:30:45","foo-123","foo-1234","foo-123","foo-123 foo-1235",,,,
"foobar4.test","ns1.dns.test","ns2.dns.test","ns3.dns.test","ns4.dns.test","2018-05-30 12:30:45","foo-123","foo-1234","foo-123","foo-123",,,,
```

Handles used in the example data above, *hdl.csv*:

```
"handle","name","address","state","zip","city","country","email","phone","fax"
"foo-123","Foo Bar","At Home 123","Foostate",12345,"Foocity","Fooland","foo@bar.test","+555.123-345-123",0
"foo-1234","Alice Bob","742 Evergreen Terrace","Foostate","12345","Foocity","Fooland","alice@bar.test","+555.123-345-678","0"
"foo-1235","Bob Alice","Escaped ""Quote"" Street 42","Barstate","54321","Barcity","Barland","bob@bar.test","+555.123-345-345","0"
```