[![pipeline status](https://gitlab.com/team-escrow/escrow-rde-client/badges/master/pipeline.svg)](https://gitlab.com/team-escrow/escrow-rde-client/commits/master)
[![License: LGPL v3](https://img.shields.io/badge/License-LGPL%20v3-blue.svg)](https://www.gnu.org/licenses/lgpl-3.0)
# escrow-rde-client
The escrow-rde-client tool helps customers of Data Escrow Agents to validate, prepare and upload their deposits.

This tool is to be employed within [ICANN's Registrar Data Escrow Program](https://www.icann.org/resources/pages/registrar-data-escrow-2015-12-01-en), has been developed by [DENIC eG](https://www.denic.de/escrow) for its own use but can be used with any other Escrow Agent.

It will take care of the following tasks:
* validate the RDE deposit files according to ICANN validation rules,
* create hashfile over the content of the delivery,
* compress, encrypt and sign the deposit files, and
* upload the files to the Data Escrow Agent.

## Getting started

You can build the tool yourself or download one of the precompiled binaries (in this case skip the `Build` section)

### Prerequisites
* Have a working golang installation (only needed for building the tool). If you don't have it check [here](https://golang.org/doc/install).
* A GPG key which is known by the escrow agent (for deposit signature)
* An SSH key which is known by the escrow agent (for sftp authentication)
* The GPG key of the escrow agent (for deposit encryption)

### Build
If you are working on a linux host, you can use ```make``` to build and install the tool:
```
make build
```
The result will be located in ```build```.

In case you want to build the tool manually:
```
# go get -u gitlab.com/team-escrow/escrow-rde-client
# mkdir build
# cd cmd/escrow-rde-client
# go build -o build/escrow-rde-client .
```

### Prepare the configuration file
* Generate a configfile with ```./escrow-rde-client -i```
* rename the generated configfile to ```config.yaml``` and adjust the parameters

### Prepare your deposit files
* to upload a deposit for the **current day (UTC time)** the source filenames (.csv) can have any filename as long
as the name includes `full`, `inc` and `hdl` (denoting full-deposit, incremental deposit and handle file)
* to upload a deposit for any past day, the filenames must follow this convention:
```
<IANAID>_RDE_<YYYY>-<MM>-<DD>_full.csv (or) <IANAID>_RDE_<YYYY>-<MM>-<DD>_inc.csv
<IANAID>_RDE_<YYYY>-<MM>-<DD>_hdl.csv
```
Replace your IANA ID, and year/month/day.

### Splitted deposit files
Source files with more than 1 million rows or 1 gigabyte of size need to be splitted into a sequence of files. The consecutive sequence numbers start with 1 and are contained in the filename by the following convention:
```
<IANAID>_RDE_<YYYY>-<MM>-<DD>_full_#.csv (or) <IANAID>_RDE_<YYYY>-<MM>-<DD>_inc_#.csv
<IANAID>_RDE_<YYYY>-<MM>-<DD>_hdl_#.csv
```
The symbol # is replaced by the corresponding sequence numbers. Only the first file should contain row headers, all other files just start with the successive content lines. To enable splitted deposits you need to update your configuration file:
```
multi: true
```

### Run it
```
# ./build/escrow-rde-client -c config.yaml
```
You can additionally use the `-s` flag to only print the last validation error message. To print validation output directly to a file, add the following configuration line to your yaml config file:
```
logFile: /path/to/your/log/file
```

## Troubleshooting

This section discusses some known problems and possible solutions.

### Crashes due to out-of-memory errors
Out-of-memory errors can cause the application to crash without further error messages. If you run low on memory during computation, consider activating the file system cache in the configuration file:
```
useFileSystemCache: true
```
This will drastically reduce the amount of memory required but also increases computation time.

## License
escrow-rde-client is licensed under the terms of the GNU Lesser General Public License v3.0.
See the repositories' [LICENSE](https://gitlab.com/team-escrow/escrow-rde-client/raw/master/LICENSE) file for further information.

