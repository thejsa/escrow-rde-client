/*
 Copyright (C) 2018 DENIC eG

 This file is part of escrow-rde-client.

	escrow-rde-client is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	escrow-rde-client is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with escrow-rde-client.  If not, see <https://www.gnu.org/licenses/>.
*/

package validation

import (
	"fmt"
	"log"
	"regexp"
	"strings"
	"time"

	"gitlab.com/team-escrow/escrow-rde-client/internal/datastructures"

	"gitlab.com/team-escrow/escrow-rde-client/internal/deposits"
	"gitlab.com/team-escrow/escrow-rde-client/internal/errors"
)

var (
	// MandatoryDomainKeys state the keys which must be present in the domain deposit header
	MandatoryDomainKeys = []string{"domain", "expir", "rt", "ac", "tc", "bc"}
	// MandatoryNameserverKeys state the valid nameserver fields which must be present in the domain deposit header
	MandatoryNameserverKeys = []string{"nameserver", "ns"}

	// global regex-pattern for domain verification
	domainRegEx *regexp.Regexp
)

func init() {
	// initialize the domain regex-pattern globally
	domainRegEx = regexp.MustCompile("^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\\-]*[A-Za-z0-9\\.])$")
}

func loadAndCheckDomains(di *deposits.DepositInfo, handles *datastructures.CsvCache, reporter Reporter, options Options) (*datastructures.CsvCache, error) {
	checkDomainHeaders(di, reporter)

	validator, err := newDomainCsvValidator(di, handles, reporter)
	if err != nil {
		return nil, err
	}

	processor, err := newCsvProcessor(di.GetDomainHeaders(), validator, options)
	if err != nil {
		processor.GetCache().Close()
		return nil, err
	}

	if err := processor.Validate(di.GetDomainCsvFiles(), reporter); err != nil {
		processor.GetCache().Close()
		return nil, err
	}

	return processor.GetCache(), nil
}

type domainCsvValidator struct {
	expiresColumnIndex int
	handleColumns      []int
	handles            *datastructures.CsvCache
	reporter           Reporter
}

func (validator *domainCsvValidator) ReportFileBegin(filePath string) {
	log.Printf("Process domain file '%v'\n", filePath)
}
func (validator *domainCsvValidator) ValidateRow(values []string) error {
	// perform domain validation
	checkValidDomainName(values[0], validator.reporter)
	checkDomainExpiry(values[0], values[validator.expiresColumnIndex], validator.reporter)
	if err := checkHandlesOfDomains(values, validator.handleColumns, validator.handles, validator.reporter); err != nil {
		return err
	}
	return nil
}
func (validator *domainCsvValidator) ReportDuplicateEntry(values []string) {
	putError(validator.reporter, errors.ValidateMultipleEntryError{
		Msg: fmt.Sprintf("Domain '%v' is listed multiple times", values[0])})
}

func newDomainCsvValidator(di *deposits.DepositInfo, handles *datastructures.CsvCache, reporter Reporter) (*domainCsvValidator, error) {
	// cache some values
	expiresColumnIndex, err := di.FindDomainColumnIndex("expir")
	if err != nil {
		return nil, err
	}

	var handleColumns []int
	for i, header := range di.GetDomainHeaders() {
		if !strings.HasPrefix(header, "p") && strings.HasSuffix(header, "-handle") {
			handleColumns = append(handleColumns, i)
		}
	}

	return &domainCsvValidator{expiresColumnIndex, handleColumns, handles, reporter}, nil
}

// check if the headers of the domain deposit contain all mandatory contact keys (rt,ac,tc,bc) and if
// nameservers and expiration date is present.
func checkDomainHeaders(di *deposits.DepositInfo, reporter Reporter) {
	for _, domainkey := range di.GetDomainHeaders() {
		if strings.Contains(domainkey, " ") {
			putError(reporter,
				errors.ValidateInvalidHeaderFormatError{
					Msg: fmt.Sprintf("Invalid header '%s' (chapter 4.1.13)", domainkey),
				})
			continue
		}
	}

	for _, checkkey := range MandatoryDomainKeys {
		found := false
		for _, domainkey := range di.GetDomainHeaders() {
			if strings.HasPrefix(strings.ToLower(domainkey), checkkey) {
				found = true
				break
			}
		}
		if !found {
			putError(reporter,
				errors.ValidateMissingHeaderFieldError{
					Msg: fmt.Sprintf("Missing required field prefixed with '%s'.", checkkey),
				})
		}
	}

	found := false
	for _, nskey := range MandatoryNameserverKeys {
		for _, key := range di.GetDomainHeaders() {
			if strings.HasPrefix(strings.ToLower(key), nskey) {
				found = true
				break
			}
		}
	}
	if !found {
		putError(reporter,
			errors.ValidateNameserverFieldError{
				Msg: "Missing required field containing nameserver information.",
			})
	}
}

// checks if a domain name consists of allowed characters only
func checkValidDomainName(domainName string, reporter Reporter) {
	if !domainRegEx.MatchString(domainName) {
		putError(reporter,
			errors.ValidateNoValidDomainNameError{
				Msg: fmt.Sprintf("Not a valid domainname: '%s'", domainName),
			})
	}
}

// checks the expiration date of the domains in the domain deposit.
func checkDomainExpiry(domainName string, expirationStr string, reporter Reporter) {
	now := time.Now().UTC()

	// here comes the tricky part. the specs from ICANN doesn't describe the format of the timestamp.
	// for dates with daytime, we only allow "yyyy-mm-dd HH:MM:SS" and "yyyy-mm-ddTHH:MM:SSZ"
	// however, as some customers only supply "yyyy-mm-dd", we accept that as well.

	expires, err := time.Parse("2006-01-02 15:04:05", expirationStr)
	if err != nil {
		expires, err = time.Parse(time.RFC3339, expirationStr)
		if err != nil {
			expires, err = time.Parse("2006-01-02", expirationStr)
			if err != nil {
				putError(reporter,
					errors.ValidateInvalidExpiryDateFormatError{
						Msg: fmt.Sprintf("Cannot parse expire date: %s, see RFC 3339 for valid formatting", expirationStr),
					})
				return
			}
			putInfo(reporter, "being graceful on date-only expiry timestamp for domain: %s", domainName)
		}
	}

	if expires.Before(now) {
		putInfo(reporter, "'%s' has expired on %s", domainName, expires.String())
	}
}

// checks if a handle states in the domains deposit is present in the handles deposit. If all stated handles are found
// nil is returned. If a handle is missing, a ValidateHandleNotFoundError is returned.
func checkHandlesOfDomains(values []string, handleColumns []int, handles *datastructures.CsvCache, reporter Reporter) error {
	for _, i := range handleColumns {
		if len(values[i]) < 1 {
			// ignore empty columns
			continue
		}

		// we need to check if more records are stated in the handle column. These can be split by " "
		valueParts := strings.Split(values[i], " ")
		for _, handle := range valueParts {
			contains, err := handles.ContainsKey(handle)
			if err != nil {
				return err
			} else if !contains {
				putError(reporter,
					errors.ValidateHandleNotFoundError{
						Msg: fmt.Sprintf("Unknown handle (value|column|domain): %v|%v|%v", handle, (i + 1), values[0]),
					})
			}
		}
	}
	return nil
}
