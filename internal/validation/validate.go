/*
 Copyright (C) 2018 DENIC eG

 This file is part of escrow-rde-client.

	escrow-rde-client is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	escrow-rde-client is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with escrow-rde-client.  If not, see <https://www.gnu.org/licenses/>.
*/

package validation

import (
	"log"

	"gitlab.com/team-escrow/escrow-rde-client/internal/datastructures"
	"gitlab.com/team-escrow/escrow-rde-client/internal/deposits"
	"gitlab.com/team-escrow/escrow-rde-client/internal/errors"
)

// Options represents configurable parameters for the validation process.
type Options struct {
	UseFileSystemCache bool
	MaximumCsvRows     int
	MaximumCsvSize     uint64
}

// NewOptions returns ValidationOptions with default parameters.
func NewOptions(fsCache bool) Options {
	return Options{fsCache, 1000000, 1024 * 1024 * 1024}
}

// ValidateDeposit performs a full domain and handle validation of the given deposit and writes the result to the reporter.
func ValidateDeposit(di *deposits.DepositInfo, reporter Reporter, options Options) error {
	var handles *datastructures.CsvCache
	if di.RequiresHandles() {
		if !di.HasHandleFiles() {
			return errors.ValidateMissingHandleFileError{Msg: "Deposit requires handles but no hdl file was found"}
		}

		hdl, err := loadAndCheckHandles(di, reporter, options)
		if err != nil {
			return err
		}
		defer hdl.Close()

		handles = hdl
	}

	domains, err := loadAndCheckDomains(di, handles, reporter, options)
	if err != nil {
		return err
	}
	defer domains.Close()

	if handles != nil {
		log.Printf("Processed %v domains and %v handles\n", domains.GetCount(), handles.GetCount())
	} else {
		log.Printf("Processed %v domains (no handles)\n", domains.GetCount())
	}

	return nil
}
