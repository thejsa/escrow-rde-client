/*
 Copyright (C) 2018 DENIC eG

 This file is part of escrow-rde-client.

	escrow-rde-client is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	escrow-rde-client is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with escrow-rde-client.  If not, see <https://www.gnu.org/licenses/>.
*/

package validation

import (
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/team-escrow/escrow-rde-client/internal/deposits"
	"gitlab.com/team-escrow/escrow-rde-client/internal/errors"
)

var (
	validDomainHeader     = "domain,expiry_date,nameserver,nameserver2,rt-handle,ac-handle,tc-handle,bc-handle"
	validDomainLine       = "test.abc,2038-06-19 13:00:00,ns1.bla.foo,ns2.bla.foo,ABC-123,ABC-123,ABC-123,ABC-123"
	validDomainLine2      = "test2.abc,2038-06-19 13:00:00,ns1.bla.foo,ns2.bla.foo,ABC-123,ABC-123,ABC-123,ABC-123"
	validDomainLine3      = "test3.abc,2038-06-19 13:00:00,ns1.bla.foo,ns2.bla.foo,ABC-123,ABC-123,ABC-123,ABC-123"
	validDomainLine4      = "test4.abc,2038-06-19 13:00:00,ns1.bla.foo,ns2.bla.foo,ABC-123,ABC-123,ABC-123,ABC-123"
	invalidDomainNameLine = "----test.abc,2038-06-19 13:00:00,ns1.bla.foo,ns2.bla.foo,ABC-123,ABC-123,ABC-123,ABC-123"
	shortDomainLine       = "test.abc,2038-06-19 13:00:00,ns1.bla.foo,ns2.bla.foo,ABC-123,ABC-123"
	badHandleDomainLine   = "badhandle.abc,2038-06-19 13:00:00,ns1.bla.foo,ns2.bla.foo,ABC-133,ABC-123,ABC-123,ABC-123"
	emptyHandleDomainLine = "test.abc,2038-06-19 13:00:00,ns1.bla.foo,ns2.bla.foo,ABC-123,ABC-123,,ABC-123"
	gracefulTimestampLine = "graceful.ts,2016-06-19,ns1.bla.foo,ns2.bla.foo,ABC-123,ABC-123,ABC-123,ABC-123"
	badexpiryDomainLine   = "badexpiry.abc,2016-0619 13:00:00,ns1.bla.foo,ns2.bla.foo,ABC-123,ABC-123,ABC-123,ABC-123"

	invalidDomainHeader     = "domain,expiry date,nameserver,nameserver2,rt-handle,tc-handle,ac-handle,bc-handle"
	missingDomainHeader     = "domain,expiry_date,nameserver,nameserver2,tc-handle,ac-handle,bc-handle"
	missingNameserverHeader = "domain,expiry_date,rt-handle,ac-handle,tc-handle,bc-handle"

	validHandleHeader = "handle,name,address,email,phone,fax"
	validHandleLine   = "ABC-123,Mr. Letter Alphabet,ABC Street 123,abc@abc.abc,+1.312.1234532,"
	validHandleLine2  = "ABC-124,Mr. Letter Alphabet,ABC Street 123,abc@abc.abc,+1.312.1234532,"
	validHandleLine3  = "ABC-125,Mr. Letter Alphabet,ABC Street 123,abc@abc.abc,+1.312.1234532,"
	validHandleLine4  = "ABC-126,Mr. Letter Alphabet,ABC Street 123,abc@abc.abc,+1.312.1234532,"

	invalidHandleHeader  = "handle,name,address,email,phone,fyx"
	invalidHandleHeader2 = "candle,name,address,email,phone,fax"
	invalidHandleLine    = ",Mr. Letter Alphabet,ABC Street 123,abc@abc.abc,+1.312.1234532,"
	shortHandleLine      = "ABC-123,Mr. Letter Alphabet,ABC Street 123"
)

func TestValidDomainHeaders(t *testing.T) {
	di := makeDeposit(arr(validDomainHeader), nil, nil)
	defer deleteDeposit(di)
	reporter := &TestReporter{}
	checkDomainHeaders(di, reporter)
	assert.Equal(t, false, reporter.HasErrors())
}

func TestInvalidDomainHeaders(t *testing.T) {
	di := makeDeposit(arr(invalidDomainHeader), nil, nil)
	defer deleteDeposit(di)
	reporter := &TestReporter{}
	checkDomainHeaders(di, reporter)
	assert.Equal(t, true, reporter.HasErrors())
	assertError(t, errors.ValidateInvalidHeaderFormatError{}, reporter.lastError)
}

func TestMissingDomainHeaders(t *testing.T) {
	di := makeDeposit(arr(missingDomainHeader), nil, nil)
	defer deleteDeposit(di)
	reporter := &TestReporter{}
	checkDomainHeaders(di, reporter)
	assert.Equal(t, true, reporter.HasErrors())
	assertError(t, errors.ValidateMissingHeaderFieldError{}, reporter.lastError)
}

func TestMissingNameserverHeaders(t *testing.T) {
	di := makeDeposit(arr(missingNameserverHeader), nil, nil)
	defer deleteDeposit(di)
	reporter := &TestReporter{}
	checkDomainHeaders(di, reporter)
	assert.Equal(t, true, reporter.HasErrors())
	assertError(t, errors.ValidateNameserverFieldError{}, reporter.lastError)
}

func TestValidHandleHeaders(t *testing.T) {
	di := makeDeposit(arr(validDomainHeader), nil, arr(validHandleHeader))
	defer deleteDeposit(di)
	reporter := &TestReporter{}
	checkHandleHeaders(di, reporter)
	assert.Equal(t, false, reporter.HasErrors())
}

func TestInvalidHandleHeaders(t *testing.T) {
	di := makeDeposit(arr(validDomainHeader), nil, arr(invalidHandleHeader))
	defer deleteDeposit(di)
	reporter := &TestReporter{}
	checkHandleHeaders(di, reporter)
	assert.Equal(t, true, reporter.HasErrors())
	assertError(t, errors.ValidateMissingHeaderFieldError{}, reporter.lastError)
}

func TestWrongHandleColName(t *testing.T) {
	di := makeDeposit(arr(validDomainHeader), nil, arr(invalidHandleHeader2))
	defer deleteDeposit(di)
	reporter := &TestReporter{}
	checkHandleHeaders(di, reporter)
	assert.Equal(t, true, reporter.HasErrors())
	assertError(t, errors.ValidateWrongHandleColumnNameError{}, reporter.lastError)
}

func TestEmptyHandleIdentifier(t *testing.T) {
	di := makeDeposit(arr(validDomainHeader), nil, arr(validHandleHeader, invalidHandleLine))
	defer deleteDeposit(di)
	reporter := &TestReporter{}
	if err := ValidateDeposit(di, reporter, NewOptions(false)); err != nil {
		panic(err)
	}
	assert.Equal(t, true, reporter.HasErrors())
	assertError(t, errors.ValidateEmptyHandleIdentifierError{}, reporter.lastError)
}

func TestValidDomainDeposit(t *testing.T) {
	di := makeDeposit(arr(validDomainHeader, validDomainLine), nil, arr(validHandleHeader, validHandleLine))
	defer deleteDeposit(di)
	reporter := &TestReporter{}
	if err := ValidateDeposit(di, reporter, NewOptions(false)); err != nil {
		panic(err)
	}
	assert.Equal(t, false, reporter.HasErrors())
}

func TestMissingHandleFile(t *testing.T) {
	di := makeDeposit(arr(validDomainHeader, validDomainLine), nil, nil)
	defer deleteDeposit(di)
	reporter := &TestReporter{}
	err := ValidateDeposit(di, reporter, NewOptions(false))
	assertError(t, errors.ValidateMissingHandleFileError{}, err)
}

func TestShortDomainLine(t *testing.T) {
	di := makeDeposit(arr(validDomainHeader, shortDomainLine), nil, arr(validHandleHeader, validHandleLine))
	defer deleteDeposit(di)
	reporter := &TestReporter{}
	err := ValidateDeposit(di, reporter, NewOptions(false))
	assertError(t, errors.InvalidColumnCountError{}, err)
}

func TestShortHandleLine(t *testing.T) {
	di := makeDeposit(arr(validDomainHeader, validDomainLine), nil, arr(validHandleHeader, shortHandleLine))
	defer deleteDeposit(di)
	reporter := &TestReporter{}
	err := ValidateDeposit(di, reporter, NewOptions(false))
	assertError(t, errors.InvalidColumnCountError{}, err)
}

func TestInvalidDomainName(t *testing.T) {
	di := makeDeposit(arr(validDomainHeader, invalidDomainNameLine), nil, arr(validHandleHeader, validHandleLine))
	defer deleteDeposit(di)
	reporter := &TestReporter{}
	if err := ValidateDeposit(di, reporter, NewOptions(false)); err != nil {
		panic(err)
	}
	assert.Equal(t, true, reporter.HasErrors())
	assertError(t, errors.ValidateNoValidDomainNameError{}, reporter.lastError)
}

func TestEmptyHandleDomain(t *testing.T) {
	di := makeDeposit(arr(validDomainHeader, emptyHandleDomainLine), nil, arr(validHandleHeader, validHandleLine))
	defer deleteDeposit(di)
	reporter := &TestReporter{}
	if err := ValidateDeposit(di, reporter, NewOptions(false)); err != nil {
		panic(err)
	}
	assert.Equal(t, false, reporter.HasErrors())
}

func TestMissingHandleDomain(t *testing.T) {
	di := makeDeposit(arr(validDomainHeader, badHandleDomainLine), nil, arr(validHandleHeader, validHandleLine))
	defer deleteDeposit(di)
	reporter := &TestReporter{}
	if err := ValidateDeposit(di, reporter, NewOptions(false)); err != nil {
		panic(err)
	}
	assert.Equal(t, true, reporter.HasErrors())
	assertError(t, errors.ValidateHandleNotFoundError{}, reporter.lastError)
}

func TestDoubleDomain(t *testing.T) {
	di := makeDeposit(arr(validDomainHeader, validDomainLine, validDomainLine), nil, arr(validHandleHeader, validHandleLine))
	defer deleteDeposit(di)
	reporter := &TestReporter{}
	if err := ValidateDeposit(di, reporter, NewOptions(false)); err != nil {
		panic(err)
	}
	assert.Equal(t, true, reporter.HasErrors())
	assertError(t, errors.ValidateMultipleEntryError{}, reporter.lastError)
}

func TestDoubleHandle(t *testing.T) {
	di := makeDeposit(arr(validDomainHeader), nil, arr(validHandleHeader, validHandleLine, validHandleLine))
	defer deleteDeposit(di)
	reporter := &TestReporter{}
	if err := ValidateDeposit(di, reporter, NewOptions(false)); err != nil {
		panic(err)
	}
	assert.Equal(t, true, reporter.HasErrors())
	assertError(t, errors.ValidateMultipleEntryError{}, reporter.lastError)
}

func TestGracefulDomainExpiry(t *testing.T) {
	di := makeDeposit(arr(validDomainHeader, gracefulTimestampLine), nil, arr(validHandleHeader, validHandleLine))
	defer deleteDeposit(di)
	reporter := &TestReporter{}
	if err := ValidateDeposit(di, reporter, NewOptions(false)); err != nil {
		panic(err)
	}
	assert.Equal(t, false, reporter.HasErrors())
}

func TestInvalidDomainExpiry(t *testing.T) {
	di := makeDeposit(arr(validDomainHeader, badexpiryDomainLine), nil, arr(validHandleHeader, validHandleLine))
	defer deleteDeposit(di)
	reporter := &TestReporter{}
	if err := ValidateDeposit(di, reporter, NewOptions(false)); err != nil {
		panic(err)
	}
	assert.Equal(t, true, reporter.HasErrors())
	assertError(t, errors.ValidateInvalidExpiryDateFormatError{}, reporter.lastError)
}

func TestDomainCsvTooLarge(t *testing.T) {
	di := makeDeposit(arr(validDomainHeader, validDomainLine, validDomainLine2, validDomainLine3, validDomainLine4), nil, arr(validHandleHeader, validHandleLine))
	defer deleteDeposit(di)
	reporter := &TestReporter{}
	options := NewOptions(false)
	options.MaximumCsvSize = 200 // large enough to accept handles, too small for domains
	if err := ValidateDeposit(di, reporter, options); err != nil {
		panic(err)
	}
	assert.Equal(t, true, reporter.HasErrors())
	assertError(t, errors.CsvTooLargeError{}, reporter.lastError)
}

func TestHandleCsvTooLarge(t *testing.T) {
	di := makeDeposit(arr(validDomainHeader, validDomainLine), nil, arr(validHandleHeader, validHandleLine, validHandleLine2, validHandleLine3, validHandleLine4))
	defer deleteDeposit(di)
	reporter := &TestReporter{}
	options := NewOptions(false)
	options.MaximumCsvSize = 200 // large enough to accept domains, too small for handles
	if err := ValidateDeposit(di, reporter, options); err != nil {
		panic(err)
	}
	assert.Equal(t, true, reporter.HasErrors())
	assertError(t, errors.CsvTooLargeError{}, reporter.lastError)
}

func TestDomainCsvTooManyRows(t *testing.T) {
	di := makeDeposit(arr(validDomainHeader, validDomainLine, validDomainLine2, validDomainLine3, validDomainLine4), nil, arr(validHandleHeader, validHandleLine))
	defer deleteDeposit(di)
	reporter := &TestReporter{}
	options := NewOptions(false)
	options.MaximumCsvRows = 3 // large enough to accept handles, too small for domains
	if err := ValidateDeposit(di, reporter, options); err != nil {
		panic(err)
	}
	assert.Equal(t, true, reporter.HasErrors())
	assertError(t, errors.TooManyRowsError{}, reporter.lastError)
}

func TestHandleCsvTooManyRows(t *testing.T) {
	di := makeDeposit(arr(validDomainHeader, validDomainLine), nil, arr(validHandleHeader, validHandleLine, validHandleLine2, validHandleLine3, validHandleLine4))
	defer deleteDeposit(di)
	reporter := &TestReporter{}
	options := NewOptions(false)
	options.MaximumCsvRows = 3 // large enough to accept domains, too small for handles
	if err := ValidateDeposit(di, reporter, options); err != nil {
		panic(err)
	}
	assert.Equal(t, true, reporter.HasErrors())
	assertError(t, errors.TooManyRowsError{}, reporter.lastError)
}

/* ##################### */
/* #### test helper #### */
/* ##################### */

type TestReporter struct {
	lastError  error
	errorCount int
}

func (reporter *TestReporter) ReportInfo(msg string) {
}
func (reporter *TestReporter) ReportError(err error) {
	reporter.lastError = err
	reporter.errorCount++
}
func (reporter *TestReporter) GetInfoCount() int {
	return 0
}
func (reporter *TestReporter) GetErrorCount() int {
	return 0
}
func (reporter *TestReporter) HasErrors() bool {
	return (reporter.errorCount > 0)
}
func (reporter *TestReporter) Close() {}

func writeLines(file string, lines []string) {
	if lines != nil {
		var sb strings.Builder
		for _, line := range lines {
			sb.WriteString(line)
			sb.WriteRune('\n')
		}
		if err := ioutil.WriteFile(file, []byte(sb.String()), 0644); err != nil {
			panic(err)
		}
	}
}
func makeDeposit(fullLines, incLines, hdlLines []string) *deposits.DepositInfo {
	dir, err := ioutil.TempDir("", "pkg-validation-test")
	if err != nil {
		panic(err)
	}
	writeLines(path.Join(dir, "full.csv"), fullLines)
	writeLines(path.Join(dir, "inc.csv"), incLines)
	writeLines(path.Join(dir, "hdl.csv"), hdlLines)
	di, err := deposits.NewDepositInfo(dir, false)
	if err != nil {
		panic(err)
	}
	return di
}
func deleteDeposit(di *deposits.DepositInfo) {
	os.RemoveAll(di.GetBaseDir())
}
func arr(lines ...string) []string {
	array := make([]string, len(lines))
	for i, line := range lines {
		array[i] = line
	}
	return array
}

func assertError(t *testing.T, expected error, actual error) bool {
	if fmt.Sprintf("%T", expected) != fmt.Sprintf("%T", actual) {
		assert.Fail(t, fmt.Sprintf("Errors not equal:\n  Expected: %T\n  Actual:   %T", expected, actual))
		return false
	}
	return true
}
