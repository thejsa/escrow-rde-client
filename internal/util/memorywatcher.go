/*
 Copyright (C) 2018 DENIC eG

 This file is part of escrow-rde-client.

	escrow-rde-client is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	escrow-rde-client is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with escrow-rde-client.  If not, see <https://www.gnu.org/licenses/>.
*/

package util

import (
	"context"
	"errors"
	"fmt"
	"runtime"
	"strconv"
	"sync"
	"time"
)

// MemoryWatcher is an asynchronous helper to track memory usage during execution.
type MemoryWatcher struct {
	peakMemoryUsage uint64
	Interval        time.Duration
	startTime       time.Time
	mutex           sync.Mutex
	isActive        bool
	cancelFn        context.CancelFunc
}

// NewMemoryWatcher initializes a new MemoryWatcher with the given check interval.
func NewMemoryWatcher(interval time.Duration) *MemoryWatcher {
	return &MemoryWatcher{Interval: interval}
}

// GetPeakMemoryUsage returns the peak memory usage since the watcher was started.
func (watcher *MemoryWatcher) GetPeakMemoryUsage() uint64 {
	watcher.mutex.Lock()
	defer watcher.mutex.Unlock()
	return watcher.peakMemoryUsage
}

// FormatPeakMemoryUsage returns the human readable peak memory usage.
func (watcher *MemoryWatcher) FormatPeakMemoryUsage() string {
	watcher.mutex.Lock()
	defer watcher.mutex.Unlock()
	return FormatSize(watcher.peakMemoryUsage)
}

// FormatSize returns a humand readable file size like 1,43 GB or 138 KB.
func FormatSize(size uint64) string {
	if size > 1000000000 {
		return fmt.Sprintf("%v GB", divround(size, 1024*1024*1024))
	} else if size > 1000000 {
		return fmt.Sprintf("%v MB", divround(size, 1024*1024))
	} else if size > 1000 {
		return fmt.Sprintf("%v KB", divround(size, 1024))
	}
	return fmt.Sprintf("%v Byte", size)
}
func divround(val uint64, div uint64) string {
	return strconv.FormatFloat(float64(val)/float64(div), 'f', 2, 64)
}

// IsActive returns true if the watcher is isActive.
func (watcher *MemoryWatcher) IsActive() bool {
	watcher.mutex.Lock()
	defer watcher.mutex.Unlock()
	return watcher.isActive
}

// Start begins asynchronous memory watching.
func (watcher *MemoryWatcher) Start() error {
	watcher.mutex.Lock()
	defer watcher.mutex.Unlock()

	if watcher.isActive {
		return errors.New("Watcher is already isActive")
	}
	watcher.isActive = true

	ctx, cancel := context.WithCancel(context.Background())
	watcher.cancelFn = cancel

	watcher.startTime = time.Now()
	watcher.peakMemoryUsage = GetCurrentMemoryUsage()

	ticker := time.NewTicker(watcher.Interval)

	go func() {
		for { // Wait for next interval or cancellation
			select {
			case _ = <-ticker.C:
				current := GetCurrentMemoryUsage()
				watcher.mutex.Lock()
				if current > watcher.peakMemoryUsage {
					watcher.peakMemoryUsage = current
				}
				watcher.mutex.Unlock()
			case <-ctx.Done():
				ticker.Stop()
				return
			}
		}
	}()

	return nil
}

// Stop stops the watcher but does not clear statistics.
func (watcher *MemoryWatcher) Stop() (time.Duration, error) {
	// tell the routine to abort using the previousy allocated boolean
	watcher.mutex.Lock()
	defer watcher.mutex.Unlock()

	if !watcher.isActive {
		return 0, errors.New("The watcher is not isActive")
	}
	watcher.cancelFn()
	watcher.isActive = false

	return time.Now().Sub(watcher.startTime), nil
}

// GetCurrentMemoryUsage returns the current memory usage of the application.
func GetCurrentMemoryUsage() uint64 {
	var m runtime.MemStats
	runtime.ReadMemStats(&m)
	return m.HeapAlloc + m.Alloc
}

// FormatCurrentMemoryUsage returns the human readable current memory usage.
func FormatCurrentMemoryUsage() string {
	return FormatSize(GetCurrentMemoryUsage())
}
