/*
 Copyright (C) 2018 DENIC eG

 This file is part of escrow-rde-client.

	escrow-rde-client is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	escrow-rde-client is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with escrow-rde-client.  If not, see <https://www.gnu.org/licenses/>.
*/

package datastructures

import (
	"fmt"
	"os"

	"github.com/syndtr/goleveldb/leveldb"
	"gitlab.com/team-escrow/escrow-rde-client/internal/errors"
)

// StringDB is the abstract interface for storing keyed strings in a database.
type StringDB interface {
	GetCount() int
	Add(key, str string) error
	ContainsKey(key string) (bool, error)
	Get(key string) (string, error)
	Close(delete bool) error
}

// FileSystemStringDB is a StringDB implementation which stores the data on the local file system.
type FileSystemStringDB struct {
	dir   string
	db    *leveldb.DB
	count int
}

// NewFileSystemStringDB returns a file-system mapped string database in the given directory.
func NewFileSystemStringDB(dir string) (StringDB, error) {
	db, err := leveldb.OpenFile(dir, nil)
	if err != nil {
		return nil, err
	}
	return &FileSystemStringDB{dir: dir, db: db}, nil
}

// GetDir returns the base directory of the file-system database.
func (db *FileSystemStringDB) GetDir() string {
	return db.dir
}

// GetCount returns the total number of entries in the database.
func (db *FileSystemStringDB) GetCount() int {
	return db.count
}

// Add stores a value with the given key in the database. Returns an error if the key is already stored.
func (db *FileSystemStringDB) Add(key, str string) error {
	exists, err := db.ContainsKey(key)
	if err != nil {
		return err
	} else if exists {
		return errors.MultipleEntryError{Msg: fmt.Sprintf("Entry '%v' is listed multiple times", key)}
	}

	if err := db.db.Put([]byte(key), []byte(str), nil); err != nil {
		return err
	}
	db.count++
	return nil
}

// ContainsKey returns true if the given key is stored in the database, otherwise false.
func (db *FileSystemStringDB) ContainsKey(key string) (bool, error) {
	_, err := db.db.Get([]byte(key), nil)
	if err == leveldb.ErrNotFound {
		return false, nil
	} else if err != nil {
		return false, err
	}
	return true, nil
}

// Get returns the string stored in the database for the given key. Returns an error if the key is not stored in the database.
func (db *FileSystemStringDB) Get(key string) (string, error) {
	result, err := db.db.Get([]byte(key), nil)
	if err != nil {
		if err == leveldb.ErrNotFound {
			return "", errors.EntryNotFoundError{Msg: fmt.Sprintf("Entry '%v' not found.", key)}
		}
		return "", err
	}
	return string(result), nil
}

// Close deletes all storage files from the local file-system.
func (db *FileSystemStringDB) Close(delete bool) error {
	if err := db.db.Close(); err != nil {
		return err
	}
	os.RemoveAll(db.dir)
	return nil
}

// MemoryStringDB is a StringDB implementation which stores all data in the memory.
type MemoryStringDB struct {
	list map[string]string
}

// NewMemoryStringDB returns a memory mapped string database in the given directory.
func NewMemoryStringDB() (StringDB, error) {
	return &MemoryStringDB{list: make(map[string]string)}, nil
}

// GetCount returns the total number of entries in the database.
func (db *MemoryStringDB) GetCount() int {
	return len(db.list)
}

// Add stores a value with the given key in the database. Returns an error if the key is already stored.
func (db *MemoryStringDB) Add(key, str string) error {
	exists, err := db.ContainsKey(key)
	if err != nil {
		return err
	} else if exists {
		return errors.MultipleEntryError{Msg: fmt.Sprintf("Entry '%v' is listed multiple times", key)}
	}

	db.list[key] = str
	return nil
}

// ContainsKey returns true if the given key is stored in the database, otherwise false.
func (db *MemoryStringDB) ContainsKey(key string) (bool, error) {
	_, ok := db.list[key]
	return ok, nil
}

// Get returns the string stored in the database for the given key. Returns an error if the key is not stored in the database.
func (db *MemoryStringDB) Get(key string) (string, error) {
	if val, ok := db.list[key]; ok {
		return val, nil
	}
	return "", errors.EntryNotFoundError{Msg: fmt.Sprintf("Entry '%v' not found.", key)}
}

// Close disposes all storage resources of this database.
func (db *MemoryStringDB) Close(delete bool) error {
	return nil
}
