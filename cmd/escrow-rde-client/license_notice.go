/*
 Copyright (C) 2018 DENIC eG

 This file is part of escrow-rde-client.

	escrow-rde-client is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	escrow-rde-client is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with escrow-rde-client.  If not, see <https://www.gnu.org/licenses/>.
*/

package main

const licenseNotice = `
Copyright (C) 2018 DENIC eG

This file is part of escrow-rde-client.

   escrow-rde-client is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   escrow-rde-client is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with escrow-rde-client.  If not, see <https://www.gnu.org/licenses/>.


This binary contains code from the following authors and sources:

github.com/davecgh/go-spew         Copyright (c) 2012-2016 Dave Collins <dave@davec.name>
github.com/danielb42/whiteflag     Copyright (c) 2019 Daniel Brandtner
github.com/fsnotify/fsnotify       Copyright (c) 2012 The Go Authors, fsnotify Authors
github.com/ghodss/yaml             Copyright (c) 2012 The Go Authors, 2014 Sam Ghods
github.com/golang/protobuf         Copyright (c) 2010 The Go Authors
github.com/golang/snappy           Copyright (c) 2011 The Snappy-Go Authors
github.com/hpcloud/tail            Copyright (c) 2014 ActiveState, 2015 Hewlett Packard Enterprise Development LP
github.com/kr/fs                   Copyright (c) 2012 The Go Authors
github.com/kr/text                 Copyright (c) 2012 Keith Rarick
github.com/kr/pretty               Copyright (c) 2012 Keith Rarick
github.com/kr/pty                  Copyright (c) 2011 Keith Rarick
github.com/onsi/ginkgo             Copyright (c) 2013-2014 Onsi Fakhouri
github.com/onsi/gomega             Copyright (c) 2013-2014 Onsi Fakhouri
github.com/pkg/errors              Copyright (c) 2015 Dave Cheney <dave@cheney.net>
github.com/pkg/sftp                Copyright (c) 2013 Dave Cheney
github.com/pmezard/go-difflib      Copyright (c) 2013 Patrick Mezard
github.com/sebidude/configparser   Copyright (c) 2018 Sebastian Stauch
github.com/stretchr/objx           Copyright (c) 2014 Stretchr, Inc., 2017-2018 objx contributors
github.com/stretchr/testify        Copyright (c) 2012-2018 Mat Ryer and Tyler Bunnell
github.com/syndtr/goleveldb        Copyright (c) 2012 Suryandaru Triandana <syndtr@gmail.com>
golang.org/x/crypto                Copyright (c) 2009 The Go Authors
golang.org/x/net                   Copyright (c) 2009 The Go Authors
golang.org/x/sync                  Copyright (c) 2009 The Go Authors
golang.org/x/sys                   Copyright (c) 2009 The Go Authors
golang.org/x/text                  Copyright (c) 2009 The Go Authors
golang.org/x/tools                 Copyright (c) 2009 The Go Authors
gopkg.in/check.v1                  Copyright (c) 2010-2013 Gustavo Niemeyer <gustavo@niemeyer.net>
gopkg.in/tomb.v1                   Copyright (c) 2010-2011 Gustavo Niemeyer <gustavo@niemeyer.net>
gopkg.in/yaml.v2                   Copyright (c) 2006 Kirill Simonov
`
