/*
 Copyright (C) 2018 DENIC eG

 This file is part of escrow-rde-client.

	escrow-rde-client is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	escrow-rde-client is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with escrow-rde-client.  If not, see <https://www.gnu.org/licenses/>.
*/

package processor

import (
	"crypto/sha256"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"runtime"
	"time"

	"gitlab.com/team-escrow/escrow-rde-client/config"
	"gitlab.com/team-escrow/escrow-rde-client/internal/deposits"
	"gitlab.com/team-escrow/escrow-rde-client/internal/escryptlib"
	"gitlab.com/team-escrow/escrow-rde-client/internal/ftpclient"
	"gitlab.com/team-escrow/escrow-rde-client/internal/util"
	"gitlab.com/team-escrow/escrow-rde-client/internal/validation"
)

var (
	archiveFileList []string
)

type Processor struct {
	appName    string
	appConfig  config.AppConfig
	configFile string
	workdir    string
	silent     bool
	logger     io.Writer
}

func New(appName string, logger io.Writer, appConfig config.AppConfig, configFile string, silent bool) (*Processor, error) {
	cwd, err := os.Getwd()
	if err != nil {
		return nil, err
	}
	return &Processor{
		appName:    appName,
		appConfig:  appConfig,
		configFile: configFile,
		workdir:    cwd,
		silent:     silent,
		logger:     logger,
	}, nil
}

func (p *Processor) Process() (string, error) {

	log.SetOutput(p.logger)
	log.Printf("=== %s ===", p.appName)
	log.Printf("working directory     : %s", p.workdir)
	log.Printf("using configfile      : %s", p.configFile)
	log.Printf("ianaID                : %s", p.appConfig.IanaID)
	log.Printf("depositBaseDir        : %s", p.appConfig.DepositBaseDir)
	log.Printf("runDir                : %s", p.appConfig.RunDir)
	log.Printf("compressAndEncrypt    : %t", p.appConfig.CompressAndEncrypt)
	log.Printf("uploadFiles           : %t", p.appConfig.UploadFiles)
	log.Printf("multi                 : %t", p.appConfig.Multi)
	log.Printf("useFileSystemCache    : %t", p.appConfig.UseFileSystemCache)
	log.Printf("gpgPrivateKeyPath     : %s", p.appConfig.GpgConfig.GpgPrivateKeyPath)
	log.Printf("gpgPrivateKeyPass     : %s", "(not displayed)")
	log.Printf("gpgReceiverPubKeyPath : %s", p.appConfig.GpgConfig.GpgReceiverPubKeyPath)
	log.Printf("sshHostname           : %s", p.appConfig.SshConfig.SshHostname)
	log.Printf("sshPort               : %d", p.appConfig.SshConfig.SshPort)
	log.Printf("sshUsername           : %s", p.appConfig.SshConfig.SshUsername)
	log.Printf("sshPrivateKeyPath     : %s", p.appConfig.SshConfig.SshPrivateKeyPath)
	log.Printf("sshPrivateKeyPassword : %s", "(not displayed)")
	log.Printf("sshHostPublicKeyPath  : %s", p.appConfig.SshConfig.SshHostPublicKeyPath)

	// check if the basedir really exists.
	_, err := os.Stat(p.appConfig.DepositBaseDir)
	if os.IsNotExist(err) {
		return "", fmt.Errorf("desposit directory %s does not exist: %s", p.appConfig.DepositBaseDir, err.Error())
	}

	var validationMemoryConsumption string
	watcher := util.NewMemoryWatcher(time.Second)
	watcher.Start()

	defer (func() {
		period, _ := watcher.Stop()
		log.Println("Peak Memory Usage:      ", watcher.FormatPeakMemoryUsage())
		log.Println("Validation Memory Usage:", validationMemoryConsumption)
		log.Println("Process Duration:       ", period)
	})()

	di, err := deposits.NewDepositInfo(p.appConfig.DepositBaseDir, p.appConfig.Multi)
	if err != nil {
		return "", err
	}

	if !p.appConfig.SkipValidation {
		err = p.validateDeposit(di)
		if err != nil {
			return "", err
		}
	}
	validationMemoryConsumption = watcher.FormatPeakMemoryUsage()

	runtime.GC()

	var destinationDir string
	if p.appConfig.CompressAndEncrypt {
		log.Println("compress encrypt and sign deposit files")
		err := p.compressAndEncrypt(di.GetDepositFiles())
		if err != nil {
			return "", err
		}
		destinationDir = p.workdir
	} else {
		destinationDir = p.appConfig.DepositBaseDir
	}

	runtime.GC()

	if p.appConfig.CompressAndEncrypt && p.appConfig.UploadFiles {
		log.Printf("sftp upload to: %s:%d", p.appConfig.SshConfig.SshHostname, p.appConfig.SshConfig.SshPort)
		err = p.sftpUpload()
		if err != nil {
			return "", fmt.Errorf("fail to upload files to ftp: %s", err.Error())
		}
	}

	return destinationDir, nil
}

func (p *Processor) validateDeposit(di *deposits.DepositInfo) error {
	reporter, err := p.makeReporter()
	if err != nil {
		return err
	}
	defer reporter.Close()

	options := validation.NewOptions(p.appConfig.UseFileSystemCache)
	if err := validation.ValidateDeposit(di, reporter, options); err != nil {
		return fmt.Errorf("technical error during validation: " + err.Error())
	}

	err = p.printReporter(reporter)
	if err != nil {
		return err
	}
	return nil
}

func (p *Processor) makeReporter() (validation.Reporter, error) {
	if p.silent {
		return validation.NewSilentReporter(), nil
	}
	if p.appConfig.UseFileSystemCache {
		return validation.NewFileSystemReporter()
	}
	return validation.NewMemoryReporter(), nil
}

func (p *Processor) printReporter(reporter validation.Reporter) error {
	log.Printf("Validation result: %v infos, %v errors\n", reporter.GetInfoCount(), reporter.GetErrorCount())
	if r, ok := reporter.(*validation.SilentReporter); ok {
		if reporter.HasErrors() {
			return fmt.Errorf("Last error: " + r.GetLastError().Error())
		}
	} else if r, ok := reporter.(validation.HistoryReporter); ok {
		err := r.WriteToLog()
		if err != nil {
			log.Println("Unable to output deposit validation result: " + err.Error())
		}
	}

	if reporter.HasErrors() {
		return fmt.Errorf("VALIDATION FAILED")
	} else {
		log.Println("VALIDATION SUCCESSFUL")
	}
	return nil
}

func (p *Processor) compressAndEncrypt(files []os.FileInfo) error {
	_, err := os.Stat(p.appConfig.RunDir)
	if os.IsNotExist(err) {
		err := os.Mkdir(p.appConfig.RunDir, 0755)
		if err != nil {
			return fmt.Errorf("ERROR: Cannot create run directory %s: %s", p.appConfig.RunDir, err.Error())
		}
	}

	// create a working dir
	t := time.Now().UTC()
	currentProcessingDir := fmt.Sprintf("%d-%02d-%02d-%02d%02d%02d",
		t.Year(),
		t.Month(),
		t.Day(),
		t.Hour(),
		t.Minute(),
		t.Second())
	p.workdir = p.appConfig.RunDir + "/" + currentProcessingDir
	os.Mkdir(p.workdir, 0755)
	log.Printf("directory for this run: %s", p.workdir)
	pubKeyFile, err := p.getRecvPubkeyFileHash()
	if err != nil {
		return err
	}
	log.Printf("sha256 of receivers public key file: %s", pubKeyFile)

	basefilestamp := fmt.Sprintf("%d-%02d-%02d", t.Year(), t.Month(), t.Day())
	basefilename := p.appConfig.IanaID + "_RDE_" + basefilestamp + "_"
	hashfile := p.workdir + "/" + basefilename + "hash.txt"

	for _, csvfile := range files {
		fileRegexp, err := regexp.Compile(`\d{1,4}_RDE_(\d{4}-\d{2}-\d{2})_.*(full|inc|hdl).*`)
		dontModifyFilename := fileRegexp.MatchString(csvfile.Name())
		if err != nil {
			return fmt.Errorf("failed parsing original filename: %s", err.Error())
		}

		if dontModifyFilename {
			log.Printf("filename %s is already in target format", csvfile.Name())
			basefilename = ""
			match := fileRegexp.FindStringSubmatch(csvfile.Name())
			hashfile = p.workdir + "/" + p.appConfig.IanaID + "_RDE_" + match[1] + "_" + "hash.txt"
		}

		srcFileName := p.appConfig.DepositBaseDir + "/" + csvfile.Name()

		// copy the basefile to workdir with correct naming.
		workingFileBaseName := basefilename + csvfile.Name()
		workingFileName := p.workdir + "/" + basefilename + csvfile.Name()
		log.Printf(" processing file %s as %s", filepath.Base(srcFileName), workingFileBaseName)

		log.Printf("  copy %s to %s", csvfile.Name(), workingFileName)
		if err := p.copyFileToWorkDirAndRename(srcFileName, workingFileName); err != nil {
			return fmt.Errorf("copy failed: %s", err.Error())
		}

		log.Printf("  store sha256 hash of %s in file %s", workingFileBaseName, hashfile)

		err = p.storeHashOfFile(workingFileName, hashfile)
		if err != nil {
			return fmt.Errorf("store sha256 hash failed: %s", err.Error())
		}
		if err := p.gzipAndEncryptFile(workingFileName); err != nil {
			return fmt.Errorf("gzip and encryption failed: %s", err.Error())
		}
		// remove workingFile
		err = os.Remove(workingFileName)
		if err != nil {
			return err
		}

	}
	archiveFileList = append(archiveFileList, hashfile)
	return nil
}

func (p *Processor) copyFileToWorkDirAndRename(srcfilename, dstfilename string) error {
	out, err := os.OpenFile(dstfilename, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0644)
	if err != nil {
		return fmt.Errorf("%s", err.Error())
	}
	defer out.Close()

	f, err := os.Open(srcfilename)
	if err != nil {
		return fmt.Errorf("%s", err.Error())
	}
	defer f.Close()

	if _, err = io.Copy(out, f); err != nil {
		return fmt.Errorf("%s", err.Error())
	}
	out.Sync()

	return nil
}

func (p *Processor) storeHashOfFile(filename string, hashfilename string) error {
	hasher := sha256.New()
	f, err := os.Open(filename)
	if err != nil {
		return fmt.Errorf("%s", err.Error())
	}
	defer f.Close()

	_, err = io.Copy(hasher, f)
	if err != nil {
		return fmt.Errorf("%s", err.Error())
	}
	hashfile, err := os.OpenFile(hashfilename, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		return fmt.Errorf("%s", err.Error())
	}
	defer hashfile.Close()

	hashline := fmt.Sprintf("%x %s\n", hasher.Sum(nil), filepath.Base(filename))
	hashfile.WriteString(hashline)
	hashfile.Close()

	return nil
}

func (p *Processor) gzipAndEncryptFile(srcfile string) error {
	dstfile := srcfile + ".gz.gpg"
	outfile, err := os.Create(dstfile)
	if err != nil {
		return err
	}
	// no defer here. file needs to be closed asap to write the encrypted data to it
	outfile.Close()

	if err := escryptlib.EscryptFile(srcfile, dstfile, p.appConfig.GpgConfig.GpgPrivateKeyPass, p.appConfig.GpgConfig.GpgPrivateKeyPath, p.appConfig.GpgConfig.GpgReceiverPubKeyPath); err != nil {
		return fmt.Errorf("failed to generate and write compressed/decrypted content: %s", err.Error())
	}

	log.Printf("  wrote gpg file: %s", filepath.Base(outfile.Name()))
	archiveFileList = append(archiveFileList, outfile.Name())
	return nil
}

func (p *Processor) sftpUpload() error {
	client, err := ftpclient.NewFtpClient(p.appConfig.SshConfig)
	if err != nil {
		return fmt.Errorf("failed to create sftp connection: %s", err.Error())
	}
	// Close connection
	defer client.SftpClient.Close()
	defer client.SshClient.Close()
	log.Printf(" connection established successfully")
	cwd, err := client.SftpClient.Getwd()
	log.Printf(" remote working directory: %s:%s", p.appConfig.SshConfig.SshHostname, cwd)

	for _, f := range archiveFileList {
		remotefile, err := client.SftpClient.Create(filepath.Base(f))
		defer remotefile.Close()
		if err != nil {
			return err
		}
		localfile, err := os.Open(f)
		defer localfile.Close()
		if err != nil {
			return err
		}
		log.Printf(" uploading: %s", localfile.Name())
		_, err = io.Copy(remotefile, localfile)
		if err != nil {
			return fmt.Errorf(" %s: %s", localfile.Name(), err.Error())
		}
	}
	return nil
}

func (p *Processor) getRecvPubkeyFileHash() (string, error) {
	f, err := os.Open(p.appConfig.GpgConfig.GpgReceiverPubKeyPath)
	if err != nil {
		return "", fmt.Errorf("ERROR: Cannot read %s", p.appConfig.GpgConfig.GpgReceiverPubKeyPath)
	}
	defer f.Close()

	h := sha256.New()
	io.Copy(h, f)
	return fmt.Sprintf("%x", h.Sum(nil)), nil
}
